# -*- coding: utf-8 -*-
##############################################################################
#

#
##############################################################################
from datetime import timedelta, datetime, time
import pytz
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateAction, StateTransition, \
    Button
from trytond.pyson import PYSONEncoder
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction

__all__ = ['CreateGuardiasReportStart','CreateGuardiasReportWizard']


class CreateGuardiasReportStart(ModelView):
    'Create Guardias Report - Start'
    __name__ = 'z_guardias.create.guardias.report.start'
    
    start_date = fields.DateTime('Start Date', required=True)
    end_date = fields.DateTime('End Date', required=True)
    report_ = fields.Selection([
        (None,''),
        ('create_guardias_report','Guardias Report'),
        ('create_anexo_a4','Anexo II - A4'),
        ('create_anexo_legal','Anexo II - Legal'),
        ],'Report', required=True, sort=False)
    
class CreateGuardiasReportWizard(Wizard):
    'Create Guardias Report - Wizard'
    __name__ = 'z_guardias.create.guardias.report.wizard'
    
    @classmethod
    def __setup__(cls):
        super(CreateGuardiasReportWizard,cls).__setup__()
        cls._error_messages.update({
            'end_date_before_start_date': 'The end date cannot be major thant the start date',
            })
    
    start = StateView('z_guardias.create.guardias.report.start',
                      'z_guardia.create_guardias_report_start_view',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Print Report','prevalidate','tryton-ok',default=True),
                       ])
    
    prevalidate = StateTransition()
    
    create_guardias_report = StateAction('z_guardia.report_guardias')
    
    create_anexo_a4 = StateAction('z_guardia.anexo_report_guardias_a4')
    
    create_anexo_legal = StateAction('z_guardia.anexo_report_guardias_legal')
    
    def default_start(self,fields):
        return{
            'end_date':datetime.now()
            }
    
    def transition_prevalidate(self):
        if self.start.end_date < self.start.start_date:
            self.raise_user_error('end_date_before_start_date')
        return self.start.report_
    
    def fill_data(self):
        start = self.start.start_date
        end = self.start.end_date        
        return {'start': start, 'end':end }
    
    def do_create_guardias_report(self, action):
        data = self.fill_data()
        return action, data
    
    def do_create_anexo_a4(self, action):
        data = self.fill_data()
        return action, data
    
    def do_create_anexo_legal(self, action):
        data = self.fill_data()
        return action, data
