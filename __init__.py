# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .z_guardia import *
from .report import *
from .wizard import *


def register():
  Pool.register(
    RegistroGuardia,
    CreateGuardiasReportStart,
    module='z_guardia', type_='model')
  Pool.register(
    RegistroGuardias,
    AnexoRegistroGuardias,
    module='z_guardia', type_='report')
  Pool.register(
    CreateGuardiasReportWizard,
    module='z_guardia', type_='wizard')

