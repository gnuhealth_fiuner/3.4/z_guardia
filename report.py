# -*- coding: utf-8 -*-
from trytond.report import Report
from trytond.pool import Pool

__all__ = ['RegistroGuardias',
        'AnexoRegistroGuardias']

class RegistroGuardias(Report):
    __name__ = 'report.guardias'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Guardias = pool.get('guardia.registro')
        report_context = super(RegistroGuardias, cls).get_context(records, data)
        if 'start' in data:
            report_context['objects'] = Guardias.search([
                                                        ('fecha','>=',data['start']),
                                                        ('fecha','<=',data['end'])
                                                        ])
        return report_context


class AnexoRegistroGuardias(Report):
    __name__ = 'anexo.report.guardias'

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        Guardias = pool.get('guardia.registro')
        report_context = super(AnexoRegistroGuardias, cls).get_context(records, data)
        if 'start' in data:
            report_context['objects'] = Guardias.search([
                                        ('fecha','>=',data['start']),
                                        ('fecha','<=',data['end'])
                                                    ])
        return report_context
