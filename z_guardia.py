# -*- coding: utf-8 -*-
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date
from trytond.model import ModelView, ModelSQL, fields
from trytond import backend
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Not, Bool, PYSONEncoder, Equal, And

__all__ = ['RegistroGuardia']

__metaclass__ = PoolMeta

class RegistroGuardia(ModelSQL,ModelView):
    'Registro de Guardia'

    __name__ = 'guardia.registro'

    fecha = fields.DateTime('Fecha y Hora de Registro', required=True)
    
    paciente = fields.Many2One('gnuhealth.patient','Paciente', required=True)

    dni = fields.Function(fields.Char('DNI'),'get_dni')

    def get_dni(self, paciente):
        if self.paciente:
           return self.paciente.name.ref
 
#    telefono = fields.Char('Telefono')    

    contacto = fields.Function(fields.Char('Contacto'), 'get_contacto')

    def get_contacto(self, paciente):
        if self.paciente.name.contact_mechanisms:
            return self.paciente.name.contact_mechanisms[0].value
        else:
            return ''


    edad = fields.Function(fields.Char('Edad', help="Edad"),'get_patient_age')

    def get_patient_age(self, paciente):
        fecha_ent = self.fecha
        if self.paciente.name.dob:
            nac = self.paciente.name.dob
            delta = relativedelta(fecha_ent, nac)
            edad = str(delta.years) + 'a ' + str(delta.months) + 'm ' + str(delta.days) + 'd'
        else:
            edad = ''
        return edad
    
    residencia = fields.Function(fields.Char('Residencia'), 'get_residencia')

    def get_residencia(self, paciente):
       if self.paciente.name.addresses[0].city:
           return self.paciente.name.addresses[0].city
       else:
           return None

    obra_soc = fields.Function(fields.Char('Obra  Social'), 'get_obra')

    def get_obra(self, name):
        res = ''
        if self.paciente.current_insurance:
            res = self.paciente.current_insurance.company.name
        return res



    diagnostico =  fields.Many2One('gnuhealth.pathology', "Diagnostico según CIE-10",
				   help="Diagnostico Presuntivo",
				   states={'invisible':Not(Equal(Eval('elegir_diagnostico'),'CIE-10'))})
    
    descrip_diagnostico = fields.Char("Descriptivo diagnóstico",
				    states={'invisible':Not(Equal(Eval('elegir_diagnostico'),'Libre'))})
    
    elegir_diagnostico = fields.Selection([
      (None,''),
      ('CIE-10',"Según CIE-10"),
      ('Libre',"Libre texto"),
      ],"Elegir tipo de Diagnóstico",sort=False,required=True)

    medico = fields.Many2One('gnuhealth.healthprofessional', 'Medico', help='Nombre del medico que realiza la atencion')

    medicacion = fields.Char('Medicacion')
    
    prestacion_concepto = fields.Char('Concepto')
    
    @staticmethod
    def default_prestacion_concepto():
      return 'Consulta en guardia'

